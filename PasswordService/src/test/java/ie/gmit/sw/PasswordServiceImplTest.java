package ie.gmit.sw;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertTrue;

class PasswordServiceImplTest {

//    private static final String IP = "52.215.205.205";
    private static final String IP = "localhost";


    @Test
    void hash() {
//        var client = new PasswordClient("localhost", 50551);
        var client = new PasswordClient(IP, 50551);
        var pass = "password";

        var res = client.getHash(123, pass);
//        System.out.println("res:" + res);

        var isOk = client.validatePassword(
                pass,
                res.getSalt(),
                res.getHashedPassword()
        ).getValue();

//        System.out.println("isOk: " + (isOk ? "ok" : "no"));
        assertTrue(isOk);
    }

    @Test
    void validator() {
        var client = new PasswordClient(IP, 50551);

        IntStream.range(0, 20)
                .forEach(it -> {
                    var password = Passwords.generateRandomPassword(20);
                    var hashResponse = client.getHash(123, password);
                    var salt = hashResponse.getSalt();
                    var hash = hashResponse.getHashedPassword();

                    assertTrue(client.validatePassword(password, salt, hash).getValue());
                });
    }

}