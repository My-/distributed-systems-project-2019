package ie.gmit.sw;

import com.google.protobuf.BoolValue;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class PasswordClient {
    private static final Logger LOG = LoggerFactory.getLogger(PasswordClient.class);
    private final PasswordServiceGrpc.PasswordServiceBlockingStub syncPasswordService;

    /**
     * Creates *blocking* client.
     * @param host ip address
     * @param port host number
     */
    PasswordClient(String host, int port) {
        ManagedChannel channel = ManagedChannelBuilder
                .forAddress(host, port)
                .usePlaintext()
                .build();
        syncPasswordService = PasswordServiceGrpc.newBlockingStub(channel);
        LOG.info("Creating PasswordClient.. Done!");
    }

    /**
     * Hashes password on the server.
     * @param userId user id
     * @param password user password
     * @return hashed and salted password
     */
    HashResponse getHash(int userId, String password) {
        LOG.debug("Hashing password: userId: {}, password: {}", userId, password);
        var hashRequest = HashRequest.newBuilder()
                .setUserId(userId)
                .setPassword(password)
                .build();
        var res = syncPasswordService.hash(hashRequest);
        LOG.info("Hashing password.. Done!");
        return res;
    }

    /**
     * Validates password on the server.
     * @param password to validate
     * @param salt password salt
     * @param hash password hash
     * @return true if password is matches the hash
     */
    BoolValue validatePassword(String password, ByteString salt, ByteString hash) {
        LOG.debug("Validating password: password: {}, salt: {}, hash: {}",
                password, salt, hash);
        var validateRequest = ValidateRequest.newBuilder()
                .setPassword(password)
                .setSalt(salt)
                .setHashedPassword(hash)
                .build();
        var res = syncPasswordService.validate(validateRequest);
        LOG.info("Validating password.. Done!");
        return res;
    }
}
