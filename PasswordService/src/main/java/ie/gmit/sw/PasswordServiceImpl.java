package ie.gmit.sw;

import com.google.protobuf.BoolValue;
import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class PasswordServiceImpl extends PasswordServiceGrpc.PasswordServiceImplBase {
    private static final Logger LOG = LoggerFactory.getLogger(PasswordServiceImpl.class);

    @Override
    public void hash(HashRequest request, StreamObserver<HashResponse> responseObserver) {
        LOG.info("Hashing password...");

        var salt = Passwords.getNextSalt();
        var hash = Passwords.hash(request.getPassword().toCharArray(), salt);

        LOG.debug("userId: {}, password: {}, hash: {}",
                request.getUserId(), request.getPassword(), hash);

        var response = HashResponse.newBuilder()
                .setUserId(request.getUserId())
                .setHashedPassword(ByteString.copyFrom(hash))
                .setSalt(ByteString.copyFrom(salt))
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();

        LOG.info("... completed!");
    }

    @Override
    public void validate(ValidateRequest request, StreamObserver<BoolValue> responseObserver) {
        LOG.info("Validating password...");
        LOG.debug("Password: {}", request.getPassword());

        var isMatch = Passwords.isExpectedPassword(
                request.getPassword().toCharArray(),
                request.getSalt().toByteArray(),
                request.getHashedPassword().toByteArray()
        );
        var response = BoolValue.newBuilder()
                .setValue(isMatch)
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();

        LOG.info("\t... {}", (isMatch ? "OK" : "INVALID"));
    }
}
