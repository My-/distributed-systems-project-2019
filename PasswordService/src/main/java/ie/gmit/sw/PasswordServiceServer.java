package ie.gmit.sw;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;

public class PasswordServiceServer {
    private static final Logger LOG = LoggerFactory.getLogger(PasswordServiceServer.class);
    private static final int DEFAULT_PORT = 50551;
    private final int port;

    private Server grpcServer;

    private PasswordServiceServer(int port){
        this.port = port;
    }

    private PasswordServiceServer(){
        this.port = DEFAULT_PORT;
        LOG.info("Creating on default PORT...");
    }

    public static void main(String... args) {
        LOG.info("Server parameters: {}", Arrays.toString(args));

        PasswordServiceServer server = null;
        int port = -1;

        try {
            port = Integer.parseInt(args[0]);
            server = new PasswordServiceServer(port);
            server.start();
        }
        catch(NumberFormatException | ArrayIndexOutOfBoundsException | IOException e){
            LOG.info("Couldn't create on port: {}", port < 0 ? "" : port);
            server = new PasswordServiceServer();

            try { server.start(); }
            catch (IOException ex) { ex.printStackTrace(); }
        }

        try { server.blockUntilShutdown(); }
        catch (InterruptedException e) { e.printStackTrace(); }

        server.stop();
    }

    /**
     * Start PasswordService server.
     * @throws IOException exception
     */
    private void start() throws IOException {
        grpcServer = ServerBuilder.forPort(port)
                .addService(new PasswordServiceImpl())
                .build()
                .start();

        LOG.info("Server started, listening on {}", port);

    }

    private void stop() {
        if (grpcServer != null) {
            grpcServer.shutdown();
        }
    }

    /**
     * Await termination on the main thread since the grpc library uses daemon threads.
     */
    private void blockUntilShutdown() throws InterruptedException {
        if (grpcServer != null) {
            grpcServer.awaitTermination();
        }
    }
}
