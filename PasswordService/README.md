# Distributed Systems Project 2019

## Password Service part 1

### Mindaugas Sarskus
ID: G00339629  
Repo: [GitLab](https://gitlab.com/My-/distributed-systems-project-2019)

> **Note:** I am using `Java 11`

### Run service:
```bash
# on default port
java -jar password-service-1.0-SNAPSHOT.jar

# on custom <GRPC_PORT> port
java -jar password-service-1.0-SNAPSHOT.jar $GRPC_PORT
```

### Extras:
- Logger `slf4j`
- Test `jUnit 5` (server need to started manually)
- Port number can be passed as a parameter to program
- Basic fall-back implementation (starts on default port):
    - invalid port string
    - port in use (say 200) _noticed by Lena_
    - no port given


Problems while doing it:
- I used proto string type to store the `salt` and `hash`. But Java conversion from String to byte array and back to String didn't work as aspected. Changing to `bytes` proto type fixed the problem.