package ie.gmit.sw;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Objects;

/**
 * This User class is used to store users in the db.
 * As well it is wrapped in RequestUser.
 */
@XmlRootElement
public class User {
    @NotNull
    private Integer id;

    @NotNull
    @Length(min=2, max=255)
    private String name;

    @NotNull
    @Pattern(regexp=".+@.+\\.[a-z]+")
    private String email;

    private byte[] hashedPassword;
    private byte[] salt;

    public User() {
    }

    public User(Integer id, String name, String email){
        this(id, name, email, null, null);
    }

    public User(Integer id, String name, String email, byte[] hashedPassword, byte[] salt) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.hashedPassword = hashedPassword;
        this.salt = salt;
    }

    @XmlElement
    @JsonProperty
    public Integer getId() {
        return id;
    }

    @XmlElement
    @JsonProperty
    public String getName() {
        return name;
    }

    @XmlElement
    @JsonProperty
    public String getEmail() {
        return email;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public byte[] getHashedPassword() {
        return hashedPassword;
    }

    public byte[] getSalt() {
        return salt;
    }


    @Override
    public boolean equals(Object obj) {
        return obj instanceof User
                && Objects.nonNull(this.id)
                && Objects.nonNull(((User) obj).id)
                && this.id.equals(((User) obj).id);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", hashedPassword=" + hashedPassword +
                ", salt=" + salt +
                '}';
    }
}
