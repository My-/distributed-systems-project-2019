package ie.gmit.sw;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.util.*;

/**
 * Fake DB controller.
 */
public class StorageController {
    private static final Logger LOGGER = LoggerFactory.getLogger(StorageController.class);

    private static final Map<Integer, User> storage = new TreeMap<>();
    private static final StorageController instance = new StorageController();

    private StorageController() {
    }

    /**
     * Get singleton instance.
     * @return
     */
    public static StorageController getInstance() {
        return instance;
    }

    /**
     * Add user to DB
     * @param user
     * @return
     */
    public Optional<User> addUser(@NotNull User user) {
        LOGGER.debug("Adding user: {}", user);
        var userId = user.getId();
        var dbUser = storage.get(userId);
        return Objects.isNull(dbUser)
                ? Optional.ofNullable(storage.put(userId, user))
                            .or(() -> Optional.ofNullable(dbUser))
                : Optional.empty();
    }

    /**
     * Get single user by user id
     * @param userId
     * @return
     */
    public Optional<User> getUser(int userId) {
        LOGGER.debug("Getting user by ID: {}", userId);
        return Optional.ofNullable(storage.get(userId));
    }

    /**
     * Shallow copy.
     * @return
     */
    public List<User> getAll() {
        LOGGER.debug("Getting all user, total: {}", storage.size());
        return new ArrayList<>(storage.values());
    }

    /**
     * Updates user.
     * @param id
     * @param user
     * @return
     */
    public Optional<User> updateUser(int id, @NotNull User user) {
        LOGGER.debug("Updating user: {}", user);
        return id != user.getId()
                ? Optional.of(storage.merge(id, user, (k, v) -> user))
                : Optional.empty();
    }

    /**
     * Replace user
     * @param user
     * @return
     */
    public Optional<User> replaceUser(@NotNull User user) {
        LOGGER.debug("Replacing user: {}", user);
        return storage.get(user.getId()) == null
                ? Optional.empty()
                : Optional.ofNullable(storage.replace(user.getId(), user));
    }

    /**
     * Remove user from storage
     * @param user
     * @return
     */
    public boolean deleteUser(@NotNull User user) {
        LOGGER.debug("Deleting user: {}", user);
        return storage.remove(user.getId(), user);
    }

    /**
     * Checks DB for emptiness.
     * @return
     */
    public static boolean isEmpty(){
        return StorageController.storage.isEmpty();
    }

}
