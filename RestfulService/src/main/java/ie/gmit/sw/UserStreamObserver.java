package ie.gmit.sw;

import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.util.function.Consumer;

/**
 * Callback class. This class constructor takes function as parameter.
 * That function (onComplete) will take newly created used and executed
 * inside onComplete() method call (mini Command Pattern).
 */
public class UserStreamObserver implements StreamObserver<HashResponse> {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserStreamObserver.class);

    private User oldUser;
    private User newUser;
    private Consumer<User> onComplete;

    private UserStreamObserver(User oldUser, Consumer<User> onComplete){
        this.oldUser = oldUser;
        this.onComplete = onComplete;
    }

    /**
     * Crates {@link UserStreamObserver} (callback)
     * @param user is template for new {@link User} (deep copy)
     * @param onComplete this function will be called then stream completes
     *                   and newly crated user will be passed in to it.
     * @return instance of this callback function.
     */
    public static UserStreamObserver of(@NotNull User user,
                                        @NotNull Consumer<User> onComplete
    ){
        return new UserStreamObserver(user, onComplete);
    }

    @Override
    public void onNext(HashResponse hashResponse) {
        byte[] hash = hashResponse.getHashedPassword().toByteArray();
        byte[] salt = hashResponse.getSalt().toByteArray();
        newUser = new User(
                oldUser.getId(),
                oldUser.getName(),
                oldUser.getEmail(),
                hash, salt);
        LOGGER.debug("User was created: {}", newUser);
    }

    @Override
    public void onError(Throwable throwable) {
        LOGGER.error(throwable.getMessage());
    }

    @Override
    public void onCompleted() {
        onComplete.accept(newUser);
        LOGGER.debug("User was sored: {}", newUser);
    }
}

