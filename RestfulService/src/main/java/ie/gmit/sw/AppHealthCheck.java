package ie.gmit.sw;

import com.codahale.metrics.health.HealthCheck;

public class AppHealthCheck extends HealthCheck {
    @Override
    protected Result check() throws Exception {
        return StorageController.isEmpty()
                ? Result.unhealthy("Storage has no records!")
                : Result.healthy("OK");
    }
}
