package ie.gmit.sw;

import com.google.protobuf.BoolValue;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import java.util.concurrent.TimeUnit;

/**
 * PasswordClient class uses gRPC for method call. Here are two methods:
 *      - validate (synchronous)
 *      - hash (asynchronous)
 */
class PasswordClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(PasswordClient.class);
    private final ManagedChannel channel;
    private final PasswordServiceGrpc.PasswordServiceBlockingStub syncPasswordService;
    private final PasswordServiceGrpc.PasswordServiceStub asyncPasswordService;

    /**
     * Creates client for gRPC {@link PasswordServiceGrpc}.
     * @param host ip address of the running {@link PasswordServiceGrpc}
     * @param port host number of the running {@link PasswordServiceGrpc}
     */
    PasswordClient(String host, int port) {
        channel = ManagedChannelBuilder
                .forAddress(host, port)
                .usePlaintext()
                .build();
        syncPasswordService = PasswordServiceGrpc.newBlockingStub(channel);
        asyncPasswordService = PasswordServiceGrpc.newStub(channel);
        LOGGER.info("Creating PasswordClient.. Done!");
    }

    /**
     * Close channel;
     * @throws InterruptedException
     */
    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    /**
     * Asynchronously hashes password on the {@link PasswordServiceGrpc}.
     * @param userId user id
     * @param password user password
     * @return true if no errors was accrued.
     */
    boolean getHash(@NotNull int userId,
                    @NotNull String password,
                    @NotNull StreamObserver<HashResponse> responseObserver
    ) {
        LOGGER.debug("Hashing password: userId: {}, password: {}", userId, password);
        var hashRequest = HashRequest.newBuilder()
                .setUserId(userId)
                .setPassword(password)
                .build();

        try {
            asyncPasswordService.hash(hashRequest, responseObserver);
        }
        catch (StatusRuntimeException ex) {
            LOGGER.warn("RPC failed: {}", ex.getStatus());
            return false;
        }

        LOGGER.info("Hashing password.. Done!");
        return true;
    }

    /**
     * Synchronously validates password on the {@link PasswordServiceGrpc}.
     * @param password to validate
     * @param salt password salt
     * @param hash password hash
     * @return true if password is matches the hash
     */
    boolean validatePassword(@NotNull String password,
                             @NotNull ByteString salt,
                             @NotNull ByteString hash
    ) {
        LOGGER.debug("Validating password: password: {}, salt: {}, hash: {}",
                password, salt, hash);
        var validateRequest = ValidateRequest.newBuilder()
                .setPassword(password)
                .setSalt(salt)
                .setHashedPassword(hash)
                .build();
        var res = BoolValue.newBuilder().setValue(false).build();
        try{
            res = syncPasswordService.validate(validateRequest);
            LOGGER.info("Validating password.. Done!");
        }
        catch (StatusRuntimeException ex) {
            LOGGER.warn("RPC failed: {}", ex.getStatus());
        }

        return res.getValue();
    }
}
