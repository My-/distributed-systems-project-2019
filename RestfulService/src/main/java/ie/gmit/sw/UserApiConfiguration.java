package ie.gmit.sw;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;

public class UserApiConfiguration extends Configuration{
    private String passwordHost;
    private int passwordPort;

    @JsonProperty
    public String getPasswordHost() {
        return passwordHost;
    }

    @JsonProperty
    public int getPasswordPort() {
        return passwordPort;
    }
}
