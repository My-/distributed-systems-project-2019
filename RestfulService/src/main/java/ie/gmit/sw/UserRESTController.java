package ie.gmit.sw;

import com.google.protobuf.ByteString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * {@link UserRESTController} is Jersey REST endpoint class. This class
 * compatible with JSON an XML data.
 *
 * API documentation:
 *      https://app.swaggerhub.com/apis/example123/RESTfulService/1.0.0#/
 * Code adopted from:
 *      https://howtodoinjava.com/dropwizard/tutorial-and-hello-world-example/
 */
@Path("/users")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Singleton
public class UserRESTController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserRESTController.class);

    private final Validator validator;
    private final StorageController usersStorage;
    private final PasswordClient passwordClient;

    public UserRESTController(Validator validator, UserApiConfiguration configuration) {
        this.validator = validator;
        usersStorage = StorageController.getInstance();
        String host = configuration.getPasswordHost();
        int port = configuration.getPasswordPort();
        passwordClient = new PasswordClient(host, port);
    }

    /**
     * {@code GET}'s all {@link User}'s from storage.
     * @return All {@link User}'s stored inside storage.
     */
    @GET
    public Response getAll() {
        // https://stackoverflow.com/a/18240578/5322506
        GenericEntity<List<User>> gList = new GenericEntity<>(usersStorage.getAll()) {};
        return Response.ok(gList).build();
    }

    /**
     * {@code GET}'s {@link User} wit requested ID.
     * @param id of the user we want to retrieve.
     * @return {@link User} with the requested ID or {@link Status} code {@code NOT_FOUND}
     * if can't find {@link User} with the given ID.
     */
    @GET
    @Path("/{id}")
    public Response getUserById(@PathParam("id") Integer id) {
        LOGGER.info("Getting user by ID: {}", id);
        Optional<User> user = usersStorage.getUser(id);
        return user.isPresent()
                ? Response.ok(user.get()).build()
                : Response.status(Status.NOT_FOUND).entity("Cant find requested user").build();
    }

    /**
     * Creates new user and stores it to DB.
     * Note: uses gRPC {@link PasswordClient} to connect to the {@link PasswordServiceServer}
     * @param request {@link RequestUser} containing  {@link User} we want to store.
     * @return {@link Status} code {@code CREATED} if user was successfully created
     *                          or {@code BAD_REQUEST} if {@link RequestUser} or {@link User} has violations.
     *                          or {@code CONFLICT} if {@link User}'s id is already exist.
     */
    @POST
    public Response createUser(RequestUser request) {
        LOGGER.info("Creating user: {}", request);
        // check violations
        List<String> violations = getViolations(request);
        if(violations.size() > 0){
            LOGGER.warn("Found {} violations: {}", violations.size(), violations.toString());
            return Response.status(Status.BAD_REQUEST).entity(violations.toString()).build();
        }

        // user already exist
        var userId = request.getUser().getId();
        if(usersStorage.getUser(userId).isPresent()){
            return Response.status(Status.CONFLICT).entity("User with id: "+ userId +" already exist!").build();
        }
        // add user
        var responseObserver = UserStreamObserver.of(request.getUser(), usersStorage::addUser);
        passwordClient.getHash(request.getUser().getId(), request.getPassword(), responseObserver);

        return Response.status(Status.CREATED).entity("Asynchronous add user submitted").build();
    }

    /**
     * Updates existing user and stores it to DB.
     * Note: uses gRPC {@link PasswordClient} to connect to the {@link PasswordServiceServer}
     * @param request {@link RequestUser} containing {@link User} we want to update.
     * @return {@link Status} code {@code ACCEPTED} if user was successfully updated
     *                          or {@code BAD_REQUEST} if {@link RequestUser} or {@link User} has violations.
     *                          or {@code NOT_FOUND} if {@link User} not found in DB.
     */
    @PUT
    public Response updateUser(RequestUser request) {
        LOGGER.info("Updating user: {}", request);
        // check violations
        List<String> violations = getViolations(request);
        if(violations.size() > 0){
            LOGGER.warn("Found {} violations: {}", violations.size(), violations.toString());
            return Response.status(Status.BAD_REQUEST).entity(violations.toString()).build();
        }

        var userId = request.getUser().getId();
        if(usersStorage.getUser(userId).isEmpty()){
            return Response.status(Status.NOT_FOUND).entity("Cant find requested user").build();
        }

        var responseObserver = UserStreamObserver.of(request.getUser(), usersStorage::replaceUser);
        passwordClient.getHash(request.getUser().getId(), request.getPassword(), responseObserver);

        return Response.status(Status.ACCEPTED).entity("Asynchronous user update submitted").build();
    }

    /**
     * Removes {@link User} form the database. Here is NO authentication.
     * @param request  {@link RequestUser} containing  {@link User} we want to delete.
     * @return {@link Status} code {@code ACCEPTED} if user was successfully removed
     *                          or {@code NOT_FOUND} if failed.
     */
    @DELETE
    public Response removeUser(RequestUser request) {
        LOGGER.info("Removing user: {}", request);
        return usersStorage.deleteUser(request.getUser())
                ? Response.status(Status.ACCEPTED).entity("User was removed!").build()
                : Response.status(Status.NOT_FOUND).entity("Failed to delete user").build();
    }

    /**
     * "Logs" user in. Checks if password matches hash and salt.
     * @param request {@link RequestUser} containing  {@link User} we want to "login"
     * @return {@link Status} code {@code ACCEPTED} if {@link User} was successfully "logged" in
     *                          or {@code FORBIDDEN} if wong password
     *                          or {@code BAD_REQUEST} if {@link RequestUser} or {@link User} has violations
     *                          or {@code NOT_FOUND} if {@link User} was not found in DB
     */
    @POST
    @Path("/login")
    public Response login(RequestUser request) {
        LOGGER.info("Logging in user: {}", request);
        // check violations
        List<String> violations = getViolations(request);
        if(violations.size() > 0){
            LOGGER.warn("Found {} violations: {}", violations.size(), violations.toString());
            return Response.status(Status.BAD_REQUEST).entity(violations.toString()).build();
        }
        // no user in db
        Optional<User> userFromDB = usersStorage.getUser(request.getUser().getId());
        if (userFromDB.isEmpty()) {
            return Response.status(Status.NOT_FOUND).entity("Cant find requested user").build();
        }

        var u = userFromDB.get();
        var isValid = passwordClient.validatePassword(
                request.getPassword(),
                ByteString.copyFrom(u.getSalt()),
                ByteString.copyFrom(u.getHashedPassword())
        );

        if (isValid) {
            return Response.status(Status.ACCEPTED).entity("User are logged in!").build();
        }

        return Response.status(Status.FORBIDDEN).entity("Wrong password!").build();
    }

    /**
     * Get violation messages if any.
     * @param request {@link RequestUser} containing {@link User} we are validating.
     * @return violation messages as list.
     */
    private List<String> getViolations(RequestUser request){
        List<String> violations = validate(request);
        if(request.getUser() == null){
            violations.addAll(validate(request.getUser()));
        }

        return violations;
    }

    /**
     * Validates element.
     * @param e element to validate.
     * @param <E> Type ot element.
     * @return violation messages as list.
     */
    private <E>List<String> validate(@NotNull E e){
        Set<ConstraintViolation<E>> violations = validator.validate(e);
        Function<ConstraintViolation<E>, String> createViolationMessage = violation ->
                violation.getPropertyPath().toString() + ": " + violation.getMessage();
        return violations.stream()
                .map(createViolationMessage)
                .collect(Collectors.toList());
    }

}
