package ie.gmit.sw;

import io.dropwizard.Application;
import io.dropwizard.jetty.ConnectorFactory;
import io.dropwizard.jetty.HttpConnectorFactory;
import io.dropwizard.server.DefaultServerFactory;
import io.dropwizard.server.SimpleServerFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.UnknownHostException;
import java.util.stream.Stream;

public class App extends Application<UserApiConfiguration> {
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    @Override
    public void initialize(Bootstrap<UserApiConfiguration> b) {
    }

    @Override
    protected void bootstrapLogging() {
    }

    @Override
    public void run(UserApiConfiguration c, Environment e) throws Exception {
        var port = getPort(c);

        if( !canBind("127.0.0.1", port)){
            LOGGER.error("Can not start. Port {} is in use!", port);
            System.exit(666);
        }

        LOGGER.info("Registering REST resources");
        e.jersey().register(new UserRESTController(e.getValidator(), c));

        final AppHealthCheck healthCheck = new AppHealthCheck();
        e.healthChecks().register("example", healthCheck);
    }

    private int getPort(UserApiConfiguration c){
        // https://stackoverflow.com/a/48506154/5322506
        Stream<ConnectorFactory> connectors = c.getServerFactory() instanceof DefaultServerFactory
                ? ((DefaultServerFactory)c.getServerFactory()).getApplicationConnectors().stream()
                : Stream.of((SimpleServerFactory)c.getServerFactory()).map(SimpleServerFactory::getConnector);

        int port = connectors.filter(connector -> connector.getClass().isAssignableFrom(HttpConnectorFactory.class))
                .map(connector -> (HttpConnectorFactory) connector)
                .mapToInt(HttpConnectorFactory::getPort)
                .findFirst()
                .orElseThrow(IllegalStateException::new);

        return port;
    }

    private static boolean canBind(String host, int portNr) throws UnknownHostException {
        // https://codereview.stackexchange.com/a/219604
        var address = InetAddress.getByName(host);
        try (var ignored = new ServerSocket(portNr, 0, address)) { return true; }
        catch (IOException e) { return false; }
    }

    public static void main(String[] args) throws Exception {
         new App().run(args);
    }
}
