package ie.gmit.sw;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.*;

/**
 * Request wraps User and is used by app users to query app.
 */
@XmlRootElement(name = "request")
public class RequestUser {
    @NotNull
    private User user;

    @NotNull
    @Length(min=6, max=255)
    private String password;

    public RequestUser() {
    }

    public RequestUser(User user, String password) {
        this.user = user;
        this.password = password;
    }

    @XmlElement
    @JsonProperty
    public User getUser() {
        return user;
    }

    public void setUser(User user){
        this.user = user;
    }


    @XmlElement
    @JsonProperty
    public String getPassword() {
        return password;
    }

    public void setPassword(String password){
        this.password = password;
    }

    @Override
    public String toString() {
        return "RequestUser{" +
                "user=" + user +
                ", password='" + password + '\'' +
                '}';
    }
}
