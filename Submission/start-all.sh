#!/usr/bin/env bash

cd part_1
bash -e ./start-part-1.sh &
cd ../part_2
bash -e ./start-part-2.sh
