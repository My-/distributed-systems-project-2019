# Password Service (part-2)

### Mindaugas Sarskus
ID: G00339629  
Repo: [GitLab](https://gitlab.com/My-/distributed-systems-project-2019)

> **Note:** I am using `Java 11`

### Run service:
```bash
java -jar DropWizardExample-1.0-SNAPSHOT.jar server config.yaml
```
