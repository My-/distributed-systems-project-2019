### Mindaugas Sarskus
ID: G00339629  
Repo: [GitLab](https://gitlab.com/My-/distributed-systems-project-2019)

> **Note:** I am using `Java 11`

### Run service:
Generate jars and put them in the corresponding folder (if they absent). Then run:

```bash
# start part-1 and part-1 together
./start-all.sh
```
> **Note:** Each part cant be started separately.

### Testing:
- Import Postman test file `DS_assignment.postman_collection.json`
- Run tests. Please run few time because often at first run some test fails, I guess because of acync execution of some methods inside app.

> **Note:** SwaggerHub generated yaml has some issues and is why I created Postman specific file.
