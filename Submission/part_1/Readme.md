# Password Service (part-1)

### Mindaugas Sarskus
ID: G00339629  
Repo: [GitLab](https://gitlab.com/My-/distributed-systems-project-2019)

> **Note:** I am using `Java 11`

### Run service:
```bash
# on default port
java -jar password-service-1.0-SNAPSHOT.jar

# on custom <GRPC_PORT> port
java -jar password-service-1.0-SNAPSHOT.jar $GRPC_PORT
```

